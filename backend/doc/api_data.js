define({ "api": [
  {
    "type": "get",
    "url": "/api/todo",
    "title": "REST routings for the ToDo list",
    "name": "gettodo",
    "group": "todo",
    "sampleRequest": [
      {
        "url": "url\nREST routings for the ToDo list"
      }
    ],
    "version": "0.0.0",
    "filename": "server/server.js",
    "groupTitle": "todo"
  },
  {
    "type": "delete",
    "url": "/api/todo/:totoItem",
    "title": "REST routings for the ToDo list",
    "name": "deletetodo",
    "group": "todoitem",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "put",
            "description": "<p>something here</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/server.js",
    "groupTitle": "todoitem"
  },
  {
    "type": "post",
    "url": "/api/todo/:todoItem",
    "title": "REST routings for the ToDo list",
    "name": "posttodo",
    "group": "todoitem",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "put",
            "description": "<p>something here</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "server/server.js",
    "groupTitle": "todoitem"
  }
] });
